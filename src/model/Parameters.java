/*
 *   GameOfLife - An implementation of John H. Conway's cellular automaton.
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Parameters.java
 *
 * This class stores all of the parameters configured from the interface.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;

public class Parameters {

    private static int mGridSize = 100;
    private static double mSeedProbability = 0.5;
    private static boolean mIsToroidal = true;
    private static int mImageSize = 700;
    private static int mThreadDelay = 0;

    private static int mPixelSize = Parameters.getImageSize() / Parameters.getGridSize();

    public static int getGridSize() {
        return mGridSize;
    }

    public static double getSeedProbability() {
        return mSeedProbability;
    }

    public static boolean isToroidal() {
        return mIsToroidal;
    }

    public static int getPixelSize() {
        return mPixelSize;
    }

    public static int getImageSize() {
        return mImageSize;
    }

    public static int getThreadDelay() {
        return mThreadDelay;
    }

    public static void setGridSize( int gridSize ) {
        mGridSize = gridSize;
    }

    public static void setSeedProbability( double seedProbability ) {
        mSeedProbability = seedProbability;
    }

    public static void setIsToroidal( boolean isToroidal ) {
        mIsToroidal = isToroidal;
    }

    public static void resetPixelSize() {
        mPixelSize = Parameters.getImageSize() / Parameters.getGridSize();
    }

    public static void setImageSize( int imageSize ) {
        mImageSize = imageSize;
    }

    public static void setThreadDelay( int threadDelay ) {
        mThreadDelay = threadDelay;
    }

    public static void centreWindow( Window frame ) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = ( int )( ( dimension.getWidth() - frame.getWidth() ) / 2 );
        int y = ( int )( ( dimension.getHeight() - frame.getHeight() ) / 2 );
        frame.setLocation( x, y );
    }
}
