/*
 *   GameOfLife - An implementation of John H. Conway's cellular automaton.
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * MainThread.java
 *
 * <p>This class is the main thread that is created and run from the user
 * interface. It instantiates the Grid class and holds the data on the living
 * and dead cells.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model;

import ui.MainInterface;
import javax.swing.SwingUtilities;

public class MainThread implements Runnable {

    private MainInterface mParent;
    private Grid mGrid;

    public MainThread( MainInterface parent ) {
        mParent = parent;
        mGrid = new Grid( mParent.getGridDraw(), mParent.getGridData() );
    }

    @Override
    public void run() {
        terminate:
        do {
            int populationThisTimeStep = mGrid.updateGrid();
            repaintGridPanel();

            if( populationThisTimeStep == 0 ) {
                mParent.populationDeadMessage();
                break terminate;
            }

            if( !mParent.isThreadRunning() ) {
                break terminate;
            }

            try {
                Thread.sleep( Parameters.getThreadDelay() );
            } catch( InterruptedException ex ) {
                //do stuff
            }

        } while( mParent.isThreadRunning() );

        mGrid.drawGridPanelImage();
        repaintGridPanel();

        // Stop this thread if population dies.
        mParent.stopThread();
    }

    private void repaintGridPanel() {
        SwingUtilities.invokeLater( new Runnable() {

            @Override
            public void run() {
                // Repaint CA panels
                mParent.mGridPanel.repaint();
            }
        } );
    }
}
