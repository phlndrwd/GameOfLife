/*
 *   GameOfLife - An implementation of John H. Conway's cellular automaton.
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Grid.java
 *
 * <p>This class contains the core model logic of John Horton Conway's Game of
 * Life that determine whether or not a cell lives or dies.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model;

import draw.GridDraw;

public class Grid {

    private GridDraw mGridDraw;
    private boolean mGrid[][];
    private boolean mGridNew[][];

    public Grid( GridDraw gridDraw, boolean gridData[][] ) {
        super();
        mGridDraw = gridDraw;
        mGrid = gridData;
        mGridNew = new boolean[ Parameters.getGridSize() ][ Parameters.getGridSize() ];
    }

    private boolean applyGameRulesToNeighbours( int numberOfLivingNeighbours, boolean cellValue ) {
        // Rules for death are not necessary with the following assumption.
        boolean returnValue = false;

        if( numberOfLivingNeighbours == 3 ) {
            returnValue = true;
        } else if( cellValue == true && numberOfLivingNeighbours == 2 ) {
            returnValue = true;
        }

        return returnValue;
    }

    public int updateGrid() {
        int population = 0;

        drawGridPanelImage();

        for( int i = 0; i < Parameters.getGridSize(); i++ ) {
            for( int j = 0; j < Parameters.getGridSize(); j++ ) {
                mGridNew[ i ][ j ] = applyGameRulesToNeighbours( countLivingNeighbours( i, j ), mGrid[ i ][ j ] );

                if( mGridNew[ i ][ j ] ) {
                    population += 1;
                }
            }
        }

        for( int i = 0; i < Parameters.getGridSize(); i++ ) {
            System.arraycopy( mGridNew[ i ], 0, mGrid[ i ], 0, Parameters.getGridSize() );
        }

        return population;
    }

    private int countLivingNeighbours( int xIndex, int yIndex ) {

        int numberOfLivingNeighbours = 0;

        for( int neighbourXIndex = ( xIndex - 1 ); neighbourXIndex <= ( xIndex + 1 ); neighbourXIndex++ ) {
            for( int neighbourYIndex = ( yIndex - 1 ); neighbourYIndex <= ( yIndex + 1 ); neighbourYIndex++ ) {

                if( ( neighbourXIndex != xIndex ) || ( neighbourYIndex != yIndex ) ) {

                    if( Parameters.isToroidal() == true ) {

                        int toroidalNeighbourX = neighbourXIndex;
                        int toroidalNeighbourY = neighbourYIndex;

                        if( toroidalNeighbourX == -1 ) {
                            toroidalNeighbourX = ( Parameters.getGridSize() - 1 );
                        } else if( toroidalNeighbourX == Parameters.getGridSize() ) {
                            toroidalNeighbourX = 0;
                        }

                        if( toroidalNeighbourY == -1 ) {
                            toroidalNeighbourY = ( Parameters.getGridSize() - 1 );
                        } else if( toroidalNeighbourY == Parameters.getGridSize() ) {
                            toroidalNeighbourY = 0;
                        }

                        if( mGrid[ toroidalNeighbourX ][ toroidalNeighbourY ] == true ) {
                            numberOfLivingNeighbours += 1;
                        }

                    } else if( ( neighbourXIndex >= 0 ) && ( neighbourXIndex < Parameters.getGridSize() ) && ( neighbourYIndex >= 0 ) && ( neighbourYIndex < Parameters.getGridSize() ) ) {
                        if( mGrid[ neighbourXIndex ][ neighbourYIndex ] == true ) {
                            numberOfLivingNeighbours += 1;
                        }
                    }
                }
            }
        }

        return numberOfLivingNeighbours;
    }

    public void drawGridPanelImage() {
        mGridDraw.drawGridPanel( mGrid );
    }
}
