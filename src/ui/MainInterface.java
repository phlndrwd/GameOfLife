/*
 *   GameOfLife - An implementation of John H. Conway's cellular automaton.
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * MainInterface.java
 *
 * <p>This class creates the main user interface for the Game of Life.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package ui;

import draw.GridDraw;
import model.MainThread;
import model.Parameters;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public final class MainInterface extends JFrame {

    public Graphics mOffScreenImageGraphics;
    public boolean mGrid[][];

    private Thread mThread;
    private boolean mIsThreadRunning;

    int mCursorXCoord;
    int mCursorYCoord;

    private final AboutDialog mAboutDialog;
    private final OptionsDialog mOptionsDialog;
    private final GridDraw mGridDraw;
    private final BufferedImage mOffScreenImage;
    private final Random mRandom;

    public MainInterface() {
        mOffScreenImage = new BufferedImage( Parameters.getImageSize() + 1, Parameters.getImageSize() + 1, BufferedImage.TYPE_INT_RGB );
        mOffScreenImageGraphics = mOffScreenImage.getGraphics();
        mGrid = new boolean[ Parameters.getGridSize() ][ Parameters.getGridSize() ];

        mAboutDialog = new AboutDialog( this, true );
        mOptionsDialog = new OptionsDialog( this, true );
        mGridDraw = new GridDraw( mOffScreenImageGraphics );
        mRandom = new Random();

        mCursorXCoord = -1;
        mCursorYCoord = -1;

        initComponents();

        ImageIcon img = new ImageIcon( "icon.png" );
        setIconImage( img.getImage() );

        stopThread();

        addWindowListener( new WindowAdapter() {
            @Override
            public void windowClosing( WindowEvent e ) {
                stopThread();
            }
        } );
    }

    public void stopThread() {
        mIsThreadRunning = false;
        mOptionsMenu.setEnabled( true );
        mClearMenu.setEnabled( true );
        mRandomMenu.setEnabled( true );
        mStartMenu.setEnabled( true );
        mStopMenu.setEnabled( false );

        mGridDraw.drawGridPanel( mGrid );
        mGridPanel.repaint();
    }

    public boolean isThreadRunning() {
        return mIsThreadRunning;
    }

    public void resetGridData() {
        mGrid = new boolean[ Parameters.getGridSize() ][ Parameters.getGridSize() ];
        mGridDraw.drawGridPanel( mGrid );
        mGridPanel.repaint();
    }

    public GridDraw getGridDraw() {
        return mGridDraw;
    }

    public boolean[][] getGridData() {
        return mGrid;
    }

    public void populationDeadMessage() {
        JOptionPane.showMessageDialog( this, "No cells are alive.", "Game Over!", JOptionPane.INFORMATION_MESSAGE );
    }

    private void startThread() {
        mOptionsMenu.setEnabled( false );
        mClearMenu.setEnabled( false );
        mRandomMenu.setEnabled( false );
        mStartMenu.setEnabled( false );
        mStopMenu.setEnabled( true );
        mIsThreadRunning = true;

        mThread = new Thread( new MainThread( this ) );
        mThread.start();
    }

    private void manualGridDrawing() {
        if( mIsThreadRunning == false ) {
            Point mousePoint = mGridPanel.getMousePosition();

            if( mousePoint != null ) {
                int cursorXCoord = ( int )Math.ceil(( ( int )mousePoint.x / ( double )Parameters.getImageSize() ) * Parameters.getGridSize() ) - 1;
                int cursorYCoord = ( int )Math.ceil(( ( int )mousePoint.y / ( double )Parameters.getImageSize() ) * Parameters.getGridSize() ) - 1;

                boolean xCoordinateChanged = false;
                boolean yCoordinateChanged = false;

                if( cursorXCoord >= 0 && cursorXCoord < Parameters.getGridSize() && cursorYCoord >= 0 && cursorYCoord < Parameters.getGridSize() ) {
                    if( cursorXCoord != mCursorXCoord ) {
                        xCoordinateChanged = true;
                        mCursorXCoord = cursorXCoord;
                    }

                    if( cursorYCoord != mCursorYCoord ) {
                        yCoordinateChanged = true;
                        mCursorYCoord = cursorYCoord;
                    }

                    if( xCoordinateChanged == true || yCoordinateChanged == true ) {
                        mGrid[cursorXCoord][cursorYCoord] = !mGrid[cursorXCoord][cursorYCoord];
                    }

                    mGridDraw.drawGridPanel( mGrid );
                    mGridPanel.repaint();
                }
            }
        }
    }

    @SuppressWarnings( "unchecked" )
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mGridPanel = new GridPanel(mOffScreenImage);
        mMenuBar = new javax.swing.JMenuBar();
        mFileMenu = new javax.swing.JMenu();
        mMenuAbout = new javax.swing.JMenuItem();
        mFileMenuSeparator = new javax.swing.JPopupMenu.Separator();
        mExitMenu = new javax.swing.JMenuItem();
        mGridMenu = new javax.swing.JMenu();
        mClearMenu = new javax.swing.JMenuItem();
        mRandomMenu = new javax.swing.JMenuItem();
        mGridMenuSeparator = new javax.swing.JPopupMenu.Separator();
        mOptionsMenu = new javax.swing.JMenuItem();
        mRunMenu = new javax.swing.JMenu();
        mStartMenu = new javax.swing.JMenuItem();
        mStopMenu = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("GameOfLife");
        setResizable(false);

        mGridPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        mGridPanel.setMaximumSize(null);
        mGridPanel.setPreferredSize(new java.awt.Dimension(702, 702));
        mGridPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                mGridPanelMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                mGridPanelMouseReleased(evt);
            }
        });
        mGridPanel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                mGridPanelMouseDragged(evt);
            }
        });

        javax.swing.GroupLayout mGridPanelLayout = new javax.swing.GroupLayout(mGridPanel);
        mGridPanel.setLayout(mGridPanelLayout);
        mGridPanelLayout.setHorizontalGroup(
            mGridPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 700, Short.MAX_VALUE)
        );
        mGridPanelLayout.setVerticalGroup(
            mGridPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 700, Short.MAX_VALUE)
        );

        mFileMenu.setText("File");

        mMenuAbout.setText("About");
        mMenuAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mMenuAboutActionPerformed(evt);
            }
        });
        mFileMenu.add(mMenuAbout);
        mFileMenu.add(mFileMenuSeparator);

        mExitMenu.setText("Exit");
        mExitMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mExitMenuActionPerformed(evt);
            }
        });
        mFileMenu.add(mExitMenu);

        mMenuBar.add(mFileMenu);

        mGridMenu.setText("Grid");

        mClearMenu.setText("Clear");
        mClearMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mClearMenuActionPerformed(evt);
            }
        });
        mGridMenu.add(mClearMenu);

        mRandomMenu.setText("Randomise");
        mRandomMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mRandomMenuActionPerformed(evt);
            }
        });
        mGridMenu.add(mRandomMenu);
        mGridMenu.add(mGridMenuSeparator);

        mOptionsMenu.setText("Options");
        mOptionsMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mOptionsMenuActionPerformed(evt);
            }
        });
        mGridMenu.add(mOptionsMenu);

        mMenuBar.add(mGridMenu);

        mRunMenu.setText("Run");

        mStartMenu.setText("Start");
        mStartMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mStartMenuActionPerformed(evt);
            }
        });
        mRunMenu.add(mStartMenu);

        mStopMenu.setText("Stop");
        mStopMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mStopMenuActionPerformed(evt);
            }
        });
        mRunMenu.add(mStopMenu);

        mMenuBar.add(mRunMenu);

        setJMenuBar(mMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mGridPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mGridPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mExitMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mExitMenuActionPerformed
        System.exit( 0 );
    }//GEN-LAST:event_mExitMenuActionPerformed

    private void mStartMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mStartMenuActionPerformed
        startThread();
    }//GEN-LAST:event_mStartMenuActionPerformed

    private void mStopMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mStopMenuActionPerformed
        stopThread();
    }//GEN-LAST:event_mStopMenuActionPerformed

    private void mOptionsMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mOptionsMenuActionPerformed
        Parameters.centreWindow( mOptionsDialog );
        mOptionsDialog.setVisible( true );
    }//GEN-LAST:event_mOptionsMenuActionPerformed

    private void mClearMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mClearMenuActionPerformed
        for( int xIndex = 0; xIndex < Parameters.getGridSize(); ++xIndex ) {
            for( int yIndex = 0; yIndex < Parameters.getGridSize(); ++yIndex ) {
                mGrid[xIndex][yIndex] = false;
            }
        }

        mGridDraw.drawGridPanel( mGrid );
        mGridPanel.repaint();
    }//GEN-LAST:event_mClearMenuActionPerformed

    private void mRandomMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mRandomMenuActionPerformed
        for( int i = 0; i < Parameters.getGridSize(); ++i ) {
            for( int j = 0; j < Parameters.getGridSize(); ++j ) {
                mGrid[i][j] = mRandom.nextDouble() < Parameters.getSeedProbability();
            }
        }
        mGridDraw.drawGridPanel( mGrid );
        mGridPanel.repaint();
    }//GEN-LAST:event_mRandomMenuActionPerformed

    private void mMenuAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mMenuAboutActionPerformed
        Parameters.centreWindow( mAboutDialog );
        mAboutDialog.setVisible( true );
    }//GEN-LAST:event_mMenuAboutActionPerformed

    private void mGridPanelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mGridPanelMousePressed
        manualGridDrawing();
    }//GEN-LAST:event_mGridPanelMousePressed

    private void mGridPanelMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mGridPanelMouseDragged
        manualGridDrawing();
    }//GEN-LAST:event_mGridPanelMouseDragged

    private void mGridPanelMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mGridPanelMouseReleased
        mCursorXCoord = -1;
        mCursorYCoord = -1;
    }//GEN-LAST:event_mGridPanelMouseReleased

    public static void main( String args[] ) {
        java.awt.EventQueue.invokeLater( new Runnable() {
            @Override
            public void run() {
                MainInterface mainInterface = new MainInterface();
                mainInterface.setVisible( true );
                Parameters.centreWindow( mainInterface );
            }
        } );
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem mClearMenu;
    private javax.swing.JMenuItem mExitMenu;
    private javax.swing.JMenu mFileMenu;
    private javax.swing.JPopupMenu.Separator mFileMenuSeparator;
    private javax.swing.JMenu mGridMenu;
    private javax.swing.JPopupMenu.Separator mGridMenuSeparator;
    public javax.swing.JPanel mGridPanel;
    private javax.swing.JMenuItem mMenuAbout;
    private javax.swing.JMenuBar mMenuBar;
    private javax.swing.JMenuItem mOptionsMenu;
    private javax.swing.JMenuItem mRandomMenu;
    private javax.swing.JMenu mRunMenu;
    private javax.swing.JMenuItem mStartMenu;
    private javax.swing.JMenuItem mStopMenu;
    // End of variables declaration//GEN-END:variables
}
