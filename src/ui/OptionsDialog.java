/*
 *   GameOfLife - An implementation of John H. Conway's cellular automaton.
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Variables.java
 *
 * <p>This class produces the user interface from which the program variables
 * and parameters can be configured.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package ui;

import model.Parameters;
import java.awt.Frame;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class OptionsDialog extends javax.swing.JDialog {

    private final MainInterface mParent;

    public OptionsDialog( Frame parent, boolean modal ) {
        super( parent, modal );
        mParent = ( MainInterface )parent;
        initComponents();
        cancel();
    }

    private void saveSettings() {
        int newGridSize = Integer.parseInt( mComboGridSize.getSelectedItem().toString().substring( 0, 3 ).replaceAll( "\\s", "" ) );

        if( newGridSize != Parameters.getGridSize() ) {
            Parameters.setGridSize( newGridSize );
            Parameters.resetPixelSize();
            mParent.resetGridData();
        }
        Parameters.setSeedProbability( Double.parseDouble( mSpinnerSeedProbability.getValue().toString() ) );
        Parameters.setIsToroidal( mRadioToroid.isSelected() );
        Parameters.setThreadDelay( mSliderDelay.getValue() );
    }

    private void ok() {
        setVisible( false );
        saveSettings();
    }

    private void cancel() {
        int comboIndex = 0;

        for( int index = 0; index < mComboGridSize.getItemCount(); index++ ) {

            String selectedGridSizeOption = ( String )mComboGridSize.getItemAt( index );
            String[] selectedGridSize = selectedGridSizeOption.split( " x " );

            if( Integer.parseInt( selectedGridSize[ 0 ] ) == Parameters.getGridSize() ) {
                comboIndex = index;
                break;
            }
        }

        mComboGridSize.setSelectedIndex( comboIndex );
        mSpinnerSeedProbability.setValue( Parameters.getSeedProbability() );

        if( Parameters.isToroidal() == true ) {
            mRadioToroid.setSelected( true );
        } else {
            mRadioPlane.setSelected( true );
        }

        mSliderDelay.setValue( Parameters.getThreadDelay() );
        setVisible( false );
    }

    private void apply() {
        saveSettings();
    }

    @SuppressWarnings( "unchecked" )
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mRadioGeometryGroup = new javax.swing.ButtonGroup();
        mButtonOK = new javax.swing.JButton();
        mButtonCancel = new javax.swing.JButton();
        mButtonApply = new javax.swing.JButton();
        mGridPanelOptions = new javax.swing.JPanel();
        mComboGridSize = new javax.swing.JComboBox();
        mLabelResolution = new javax.swing.JLabel();
        mLabelGeometry = new javax.swing.JLabel();
        mRadioPlane = new javax.swing.JRadioButton();
        mRadioToroid = new javax.swing.JRadioButton();
        mLabelDelay = new javax.swing.JLabel();
        mSliderDelay = new javax.swing.JSlider();
        mSpinnerSeedProbability = new JSpinner( new SpinnerNumberModel(0.5, 0, 1, 0.01) );
        mLabelSeedProbability = new javax.swing.JLabel();

        mRadioGeometryGroup.add(mRadioToroid);
        mRadioGeometryGroup.add(mRadioPlane);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        mButtonOK.setText("OK");
        mButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonOKActionPerformed(evt);
            }
        });

        mButtonCancel.setText("Cancel");
        mButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonCancelActionPerformed(evt);
            }
        });

        mButtonApply.setText("Apply");
        mButtonApply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonApplyActionPerformed(evt);
            }
        });

        mGridPanelOptions.setBorder(javax.swing.BorderFactory.createTitledBorder("Grid"));

        mComboGridSize.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "10 x 10", "20 x 20", "50 x 50", "70 x 70", "100 x 100", "350 x 350", "700 x 700" }));

        mLabelResolution.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        mLabelResolution.setText("Resolution:");

        mLabelGeometry.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        mLabelGeometry.setText("Geometry:");

        mRadioPlane.setText("Planar");

        mRadioToroid.setSelected(true);
        mRadioToroid.setText("Toroidal");

        mLabelDelay.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        mLabelDelay.setText("Thread Delay:");

        mSliderDelay.setMaximum(1001);
        mSliderDelay.setMinimum(1);
        mSliderDelay.setValue(1);

        javax.swing.GroupLayout mGridPanelOptionsLayout = new javax.swing.GroupLayout(mGridPanelOptions);
        mGridPanelOptions.setLayout(mGridPanelOptionsLayout);
        mGridPanelOptionsLayout.setHorizontalGroup(
            mGridPanelOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mGridPanelOptionsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mGridPanelOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mGridPanelOptionsLayout.createSequentialGroup()
                        .addGroup(mGridPanelOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(mLabelDelay, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mLabelResolution, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mGridPanelOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mComboGridSize, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(mGridPanelOptionsLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(mSliderDelay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(mGridPanelOptionsLayout.createSequentialGroup()
                        .addComponent(mLabelGeometry, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(mRadioToroid, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(mRadioPlane, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        mGridPanelOptionsLayout.setVerticalGroup(
            mGridPanelOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mGridPanelOptionsLayout.createSequentialGroup()
                .addGroup(mGridPanelOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mComboGridSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelResolution))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(mGridPanelOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mSliderDelay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mGridPanelOptionsLayout.createSequentialGroup()
                        .addComponent(mLabelDelay)
                        .addGap(11, 11, 11)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(mGridPanelOptionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelGeometry)
                    .addComponent(mRadioPlane)
                    .addComponent(mRadioToroid))
                .addGap(27, 27, 27))
        );

        mLabelSeedProbability.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        mLabelSeedProbability.setText("Randomisation Seed Probability:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mGridPanelOptions, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(mButtonOK)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonCancel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonApply))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(mLabelSeedProbability, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mSpinnerSeedProbability)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mGridPanelOptions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerSeedProbability, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelSeedProbability))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mButtonOK)
                    .addComponent(mButtonCancel)
                    .addComponent(mButtonApply))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mButtonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonOKActionPerformed
        ok();
    }//GEN-LAST:event_mButtonOKActionPerformed

    private void mButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonCancelActionPerformed
        cancel();
    }//GEN-LAST:event_mButtonCancelActionPerformed

    private void mButtonApplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonApplyActionPerformed
        apply();
    }//GEN-LAST:event_mButtonApplyActionPerformed

    public static void main( String args[] ) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for( javax.swing.UIManager.LookAndFeelInfo info: javax.swing.UIManager.getInstalledLookAndFeels() ) {
                if( "Nimbus".equals( info.getName() ) ) {
                    javax.swing.UIManager.setLookAndFeel( info.getClassName() );
                    break;
                }
            }
        } catch( ClassNotFoundException ex ) {
            java.util.logging.Logger.getLogger( OptionsDialog.class.getName() ).log( java.util.logging.Level.SEVERE, null, ex );
        } catch( InstantiationException ex ) {
            java.util.logging.Logger.getLogger( OptionsDialog.class.getName() ).log( java.util.logging.Level.SEVERE, null, ex );
        } catch( IllegalAccessException ex ) {
            java.util.logging.Logger.getLogger( OptionsDialog.class.getName() ).log( java.util.logging.Level.SEVERE, null, ex );
        } catch( javax.swing.UnsupportedLookAndFeelException ex ) {
            java.util.logging.Logger.getLogger( OptionsDialog.class.getName() ).log( java.util.logging.Level.SEVERE, null, ex );
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater( new Runnable() {
            @Override
            public void run() {
                OptionsDialog dialog = new OptionsDialog( new javax.swing.JFrame(), true );
                dialog.addWindowListener( new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing( java.awt.event.WindowEvent e ) {
                        System.exit( 0 );
                    }
                } );
                dialog.setVisible( true );
            }
        } );
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton mButtonApply;
    private javax.swing.JButton mButtonCancel;
    private javax.swing.JButton mButtonOK;
    private javax.swing.JComboBox mComboGridSize;
    private javax.swing.JPanel mGridPanelOptions;
    private javax.swing.JLabel mLabelDelay;
    private javax.swing.JLabel mLabelGeometry;
    private javax.swing.JLabel mLabelResolution;
    private javax.swing.JLabel mLabelSeedProbability;
    private javax.swing.ButtonGroup mRadioGeometryGroup;
    private javax.swing.JRadioButton mRadioPlane;
    private javax.swing.JRadioButton mRadioToroid;
    private javax.swing.JSlider mSliderDelay;
    private javax.swing.JSpinner mSpinnerSeedProbability;
    // End of variables declaration//GEN-END:variables
}
