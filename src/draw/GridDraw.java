/*
 *   GameOfLife - An implementation of John H. Conway's cellular automaton.
 *   Copyright (C) 2015  Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * GridDraw.java
 *
 * <p>This class contains the objects and routines that draw the grid data onto
 * the off screen image that is subsequently drawn onto the GridPanel to produce
 * real-time output.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package draw;

import model.Parameters;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class GridDraw {

    private final Graphics mGraphics;
    private final Graphics mOffScreenImageGraphics;
    private final BufferedImage mOffScreenImage;

    public GridDraw( Graphics graphics ) {

        mOffScreenImage = new BufferedImage( Parameters.getImageSize() + 1, Parameters.getImageSize() + 1, BufferedImage.TYPE_INT_RGB );
        mOffScreenImageGraphics = mOffScreenImage.getGraphics();
        mGraphics = graphics;
    }

    public void drawGridPanel( boolean[][] gridData ) {

        mOffScreenImageGraphics.setColor( Color.black );
        mOffScreenImageGraphics.fillRect( 0, 0, ( Parameters.getGridSize() * Parameters.getPixelSize() ), ( Parameters.getGridSize() * Parameters.getPixelSize() ) );

        mOffScreenImageGraphics.setColor( Color.white );

        for( int i = 0; i < Parameters.getGridSize(); ++i ) {
            for( int j = 0; j < Parameters.getGridSize(); ++j ) {
                if( gridData[ i ][ j ] == true ) {
                    int xCoord = i * Parameters.getPixelSize();
                    int yCoord = j * Parameters.getPixelSize();
                    mOffScreenImageGraphics.fillRect( xCoord, yCoord, Parameters.getPixelSize(), Parameters.getPixelSize() );
                }
            }
        }
        mGraphics.drawImage( mOffScreenImage, 1, 1, null );
    }
}
