License
-------

    GameOfLife - An implementation of John H. Conways cellular automaton.
    Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Description
-----------

GameOfLife is an implementation of John Horton Conway's (October 1970, Scientific American 223, 120-123) cellular automaton. The game consists of a two-dimensional grid of cells that have only two states, living or dead. Based on a set of simple rules (see the file "rules.png") a cell is determined to be alive or dead on the next time step as a function of its neighbours' states. The rules are simple, but the grid exhibits complex emergent phenomena.

GameOfLife was written with the NetBeans IDE (http://netbeans.org), and contains classes and automatically generated code to instantiate the graphical user interfaces. It is recommended to use NetBeans for editing of the GUI forms. However, the JAR can be built by executing the Makefile from an appropriate command-line interface.

Files
-----

dist/ - Compiled files
	GameOfLife.jar - JAR can be run with Java 1.7 and the following command: "java -jar GameOfLife.jar".

nbproject/ - The NetBeans (v8.0) project files.

src/ - Uncompiled source
	draw/ - Classes to handle drawing and graphics; package "draw".
		GridDraw.java - A container for the graphics drawing routine.
		ValueToColour.java - Produces the various colour map options.

	model/ - The core model; package "model".
		Grid.java - A wrapper for the two-dimensional data structure that holds the state of each location of the cellular automaton.
		MainThread.java - The model thread.
		Parameters.java - A static class that holds the input parameter values.

	ui/ - The user interface classes; package "ui".
		AboutDialog.form - An IDE generated layout file for the about dialog.
		AboutDialog.java - The code associated witht he about dialog.
		GridPanel.java - Extends JPanel to customise paint behaviour.
		MainInterface.form - An IDE generated layout file for the main GUI.
		MainInterface.java - The code associated with the main GUI.
		OptionsDialog.form - An IDE generated layout file for the Options GUI.
		OptionsDialog.java - The code associated with the Options GUI.

icon.png - The icon used by the NetBeans project. This file will need to be moved and kept in the same directory as the JAR file to be correctly used.

rules.png - An explanation of the original rules that determine the dynamics of Conway's Game of Life.
screenshot.png - A screenshot of the interface.
