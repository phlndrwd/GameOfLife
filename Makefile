echo Building classes...
if [ ! -d "build" ]; then
	mkdir build
fi
find -name "*.java" > sources.list
javac -d build @sources.list
echo Classes built.

echo Building JAR...
cd build
find -name "*.class" > classes.list
if [ ! -d "../dist" ]; then
	mkdir ../dist
fi
jar cfe ../dist/GameOfLife.jar ui.MainInterface @classes.list
rm classes.list
cd ../
rm sources.list
cd dist/
echo JAR built.

echo Running JAR...
java -jar GameOfLife.jar
